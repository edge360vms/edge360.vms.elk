$NSSMBaseDir="C:\Program_Files"

Function Install-ElasticSearchService
{
    Write-Verbose "Installing Elastic Search Service"	
	& Invoke-Expression "${NSSMBaseDir}\nssm.exe install elasticsearch ""C:\Program_Files\elasticsearch-7.8.0\bin\elasticsearch.bat""" | Out-Null
    If ($? -eq $False)
    {
        Throw "Failed to install service. $?`nLastExitCode: $LastExitCode"
    }
}

# Function Install-LogStashService
# {			
    # Write-Verbose "Installing Log Stash Service"	
    # & Invoke-Expression "${NSSMBaseDir}\nssm.exe install logstash ""C:\Program_Files\LogStash\bin\logstash.bat"" ""-f config.json""" | Out-Null
    # If ($? -eq $False)
    # {
        # Throw "Failed to install service. $?`nLastExitCode: $LastExitCode"
    # }
# }

Function Install-KibanaService
{	
    Write-Verbose "Installing Kibana Service"	
    & Invoke-Expression "${NSSMBaseDir}\nssm.exe install kibana ""C:\Program_Files\kibana-7.8.0-windows-x86_64\bin\kibana.bat""" | Out-Null
    If ($? -eq $False)
    {
        Throw "Failed to install service. $?`nLastExitCode: $LastExitCode"
    }
}

Function Start-ELK
{
    Param (
        [Parameter(Mandatory=$True)]
        [string]$ServiceName
    )
    Try 
    {
        # Get the service
        Get-Service -Name $ServiceName
        Write-Verbose "Service '${ServiceName}' is already installed."
    }
    Catch 
    {
        # If an exception is thrown, the service was not found.
        If ($ServiceName -eq "elasticsearch")
        {
            Install-ElasticSearchService
        }
        # ElseIf ($ServiceName -eq "logstash")
        # {
            # Install-LogStashService
        # }
        ElseIf ($ServiceName -eq "kibana")
        {
            Install-KibanaService
        }        
    }

    $Service=(Get-Service -Name "$ServiceName")
    If ($Service.Status -ne "Running")
    {
        Write-Verbose "Initial status of service is $($Service.Status). Attempting to start." 
        & Start-Service -Name "$ServiceName"
        If ($? -eq $False)
        {
            Throw "Failed to start service. LastExitCode: $LastExitCode"
        }
    }
    Else 
    {
        Write-Verbose "Service '${ServiceName}' is already running."
    }
}

<#
.SYNOPSIS
    Ensures the services are running. Polls every 5 seconds.
#>
Function Watch-ELK
{
    While ($True)
    {
		$ES = Get-Service -Name "elasticsearch"
		# $LS = Get-Service -Name "logstash"
		$KB = Get-Service -Name "kibana"
		# $services = $ES, $LS, $KB
		$services = $ES, $KB
		foreach ($service in $services)
		{
			If ($service.Status -ne "Running")
			{
				Throw "Service '$($service.Name)' is $($service.Status). Terminating watch."			
				# Write-Verbose "Service '$($service.Name)' is $($service.Status). Terminating watch."
			}
		}
        Start-Sleep -Seconds 5
    }
}

Start-ELK "elasticsearch"

#Start-Sleep -Seconds 15
#Start-ELK "logstash"

Start-ELK "kibana"
Watch-ELK