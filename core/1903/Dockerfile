FROM edge360/vms.windowsbase:core-1903-aspnetcore-runtime-2.1

COPY ./C C:/

ENV JAVA_HOME="C:/Program_Files/jdk-14.0.1"
RUN SETX JAVA_HOME "C:/Program_Files/jdk-14.0.1" /M
RUN SETX PATH "%JAVA_HOME%/bin;%PATH%" /M

# 9200: elastic search
# 9600 - 9700 logstash
# 5601: kibana

EXPOSE 9200 5601

SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $VerbosePreference = 'Continue'; $ProgressPreference = 'SilentlyContinue'; [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12;"]

VOLUME C:/vms/data/elk

RUN Set-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\DOS Devices' -Name 'G:' -Value "\??\C:\vms\data\elk" -Type String; 

# OpenJDK

RUN Invoke-WebRequest https://download.java.net/java/GA/jdk14.0.1/664493ef4a6946b186ff29eb326336a2/7/GPL/openjdk-14.0.1_windows-x64_bin.zip -o $env:TEMP/openjdk-14.0.1_windows-x64_bin.zip; \
Expand-Archive $env:TEMP/openjdk-14.0.1_windows-x64_bin.zip -DestinationPath 'C:/Program_Files'; \
Remove-Item $env:TEMP/openjdk-14.0.1_windows-x64_bin.zip;

# Elastic Search

RUN Invoke-WebRequest https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.8.0-windows-x86_64.zip -o $env:TEMP/elasticsearch-7.8.0-windows-x86_64.zip; \
Expand-Archive $env:TEMP/elasticsearch-7.8.0-windows-x86_64.zip -DestinationPath 'C:/Program_Files'; \
Remove-Item $env:TEMP/elasticsearch-7.8.0-windows-x86_64.zip; \
Copy-Item 'C:/Program_Files/elasticsearch.yml' 'C:/Program_Files/elasticsearch-7.8.0/config';

# Log Stash: using a local archive due to the fact that LogStash 7.8.0 for Windows is missing binaries! I had to add them manually to get it working.
# If and when they fix this issue we can do the same thing with LogStash that we're doing with Elastic Search and Kibana.
# RUN Expand-Archive "C:/Program_Files/LogStash.zip" -DestinationPath 'C:/Program_Files'; \
# Remove-Item "C:/Program_Files/LogStash.zip";
# Kibana

RUN Invoke-WebRequest https://artifacts.elastic.co/downloads/kibana/kibana-7.8.0-windows-x86_64.zip -o $env:TEMP/kibana-7.8.0-windows-x86_64.zip; \
Expand-Archive $env:TEMP/kibana-7.8.0-windows-x86_64.zip -DestinationPath 'C:/Program_Files'; \
Remove-Item $env:TEMP/kibana-7.8.0-windows-x86_64.zip; \
Copy-Item 'C:/Program_Files/kibana.yml' 'C:/Program_Files/kibana-7.8.0-windows-x86_64/config';

CMD C:/entrypoint.ps1