# edge360.vms.elk

## Version Tags
This repository is pushed to [edge360/vms.elk](https://hub.docker.com/repository/docker/edge360/vms.elk).

To pull a tag from this repository,

```PowerShell
docker pull edge360/vms.elk:<tagname>
```

Tag | Description
------------ | -------------
latest | Stable release (Windows 2019)
core-1903 | [Windows 1903 base image](https://hub.docker.com/repository/docker/edge360/vms.windowsbase)
core-1809 | [Windows 2019 base image](https://hub.docker.com/repository/docker/edge360/vms.windowsbase)
core-1607 | [Windows 2016 base image](https://hub.docker.com/repository/docker/edge360/vms.windowsbase)

## Usage

### docker

```PowerShell

docker run --name "win_elk" -m 4096m -p:9200:9200 -p 5601:5601 -v "C:/vms/data/elk:C:/vms/data/elk" edge360/vms.elk
```

### docker-compose
```yaml
version: "2.4"
services:
  win_elk:
    image: edge360/vms.elk
    mem_limit: 4096m
    ports:
      - "9200:9200"
      - "5601:5601"
    volumes:
      - "C:/vms/data/elk:C:/vms/data/elk"
```


## Parameters

Parameter | Function
------------ | -------------
__ports__ | 
`-p 9200:9200` | Elastic Search default listener.
`-p 5601:5601` | Kibana default listener.

## Persistent Storage
[elasticsearch.yml](./C/Program_Files/elasticsearch.yml) is configured to use default ports and mount data and logs in C:\vms\data\elk\data and C:\vms\data\elk\log, respectively.
[kibana.yml](./C/Program_Files/kibana.yml) is configured to use default ports and settings for Elastic Search.